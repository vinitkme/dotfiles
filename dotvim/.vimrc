set rtp+=~/.vim/bundle/vundle/
 call vundle#rc()

set nornu
set nu
set numberwidth=3
set mouse=a
autocmd VimEnter * NERDTree

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle (required)
Bundle "gmarik/vundle"

" All your bundles here
Bundle "Raimondi/delimitMate"
Bundle "vim-scripts/AnsiEsc.vim.git"
Bundle "skwp/vim-ruby-conque"
Bundle "kogakure/vim-sparkup.git"
Bundle "tomtom/tcomment_vim.git"
Bundle "nelstrom/vim-markdown-preview"
Bundle "scrooloose/nerdtree.git"
Bundle "jistr/vim-nerdtree-tabs.git"
Bundle "vim-scripts/AutoTag.git"
Bundle "vim-scripts/IndexedSearch"
Bundle "scrooloose/syntastic.git"
Bundle "sjbach/lusty.git"
Bundle "tjennings/git-grep-vim"
Bundle "skwp/greplace.vim"
Bundle "tpope/vim-repeat.git"
Bundle "tpope/vim-surround.git"
Bundle "tpope/vim-rails.git"
Bundle "vim-ruby/vim-ruby.git"
Bundle "ecomba/vim-ruby-refactoring"
Bundle "vim-scripts/matchit.zip.git"
Bundle "tpope/vim-endwise.git"
Bundle "skwp/vim-html-escape"
Bundle "Shougo/neocomplcache.git"
Bundle "skwp/vim-colors-solarized"
Bundle "tpope/vim-fugitive"
Bundle "skwp/vim-git-grep-rails-partial"
Bundle "tpope/vim-unimpaired"
Bundle "tpope/vim-git"
Bundle "vim-scripts/lastpos.vim"
Bundle "sjl/gundo.vim"
Bundle "vim-scripts/sudo.vim"
Bundle "mileszs/ack.vim"
Bundle "nelstrom/vim-textobj-rubyblock"
Bundle "kana/vim-textobj-user"
Bundle "austintaylor/vim-indentobject"
Bundle "kana/vim-textobj-datetime"
Bundle "kana/vim-textobj-entire"
Bundle "mattn/gist-vim"
Bundle "godlygeek/tabular"
Bundle "AndrewRadev/splitjoin.vim"
Bundle "vim-scripts/argtextobj.vim"
Bundle "bootleq/vim-textobj-rubysymbol"
Bundle "nathanaelkane/vim-indent-guides"
Bundle "tpope/vim-haml"
Bundle "claco/jasmine.vim"
Bundle "kana/vim-textobj-function"
Bundle "kchmck/vim-coffee-script"
Bundle "wavded/vim-stylus"
Bundle "vim-scripts/Vim-R-plugin"
Bundle "kien/ctrlp.vim"
Bundle "majutsushi/tagbar.git"
Bundle "chrisbra/color_highlight.git"
Bundle "vim-scripts/camelcasemotion.git"
Bundle "garbas/vim-snipmate.git"
Bundle "MarcWeber/vim-addon-mw-utils.git"
Bundle "tomtom/tlib_vim.git"
Bundle "honza/vim-snippets.git"
Bundle "skwp/vim-conque"
Bundle "gregsexton/gitv"
Bundle "briandoll/change-inside-surroundings.vim.git"
Bundle "timcharper/textile.vim.git"
Bundle "vim-scripts/Specky.git"
Bundle "tpope/vim-bundler"
Bundle "tpope/vim-rake.git"
Bundle "skwp/vim-easymotion"
Bundle "groenewege/vim-less.git"
Bundle "mattn/webapi-vim.git"
Bundle "astashov/vim-ruby-debugger"
Bundle "aaronjensen/vim-sass-status.git"
Bundle "skwp/vim-powerline.git"
Bundle "briancollins/vim-jst"
Bundle "pangloss/vim-javascript"
Bundle "skwp/YankRing.vim"
Bundle "tpope/vim-abolish"
Bundle "jtratner/vim-flavored-markdown.git"
Bundle "xsunsmile/showmarks.git"
Bundle "digitaltoad/vim-jade.git"
Bundle "tpope/vim-ragtag"
Bundle "vim-scripts/TagHighlight.git"
Bundle "itspriddle/vim-jquery.git"
Bundle "slim-template/vim-slim.git"
Bundle "airblade/vim-gitgutter.git"
Bundle "bogado/file-line.git"
Bundle "tpope/vim-rvm.git"
Bundle 'tmhedberg/SimpylFold'
Bundle 'tComment'
Bundle 'AutoClose'
Bundle 'pylint.vim'
Bundle 'davidhalter/jedi-vim'
Bundle 'jpalardy/vim-slime'
Bundle 'kien/rainbow_parentheses.vim'
Bundle 'ervandew/supertab'
Bundle 'airblade/vim-gitgutter'
Bundle 'gmarik/vundle'
Bundle 'mileszs/ack.vim'
Bundle 'matthias-guenther/hammer.vim'
Bundle 'tsaleh/vim-align'
Bundle 'tpope/vim-endwise'
Bundle 'tpope/vim-repeat'
Bundle 'tpope/vim-speeddating'
Bundle 'tpope/vim-surround'
Bundle 'tpope/vim-unimpaired'
Bundle 'maxbrunsfeld/vim-yankstack'
Bundle 'tpope/vim-eunuch'
Bundle 'scrooloose/nerdtree'
Bundle 'kana/vim-textobj-user'
Bundle 'vim-scripts/YankRing.vim'
Bundle 'michaeljsmith/vim-indent-object'
Bundle 'Spaceghost/vim-matchit'
Bundle 'kien/ctrlp.vim'
Bundle 'vim-scripts/scratch.vim'
Bundle 'troydm/easybuffer.vim'
Bundle 'Lokaltog/vim-powerline'
Bundle 'zaiste/tmux.vim'
Bundle 'benmills/vimux'
Bundle 'majutsushi/tagbar'
Bundle 'gregsexton/gitv'
Bundle 'scrooloose/nerdcommenter'
Bundle 'sjl/splice.vim'
Bundle 'tpope/vim-fugitive'
Bundle 'scrooloose/syntastic'
Bundle 'tpope/vim-haml'
Bundle 'juvenn/mustache.vim'
Bundle 'tpope/vim-markdown'
Bundle 'digitaltoad/vim-jade'
Bundle 'bbommarito/vim-slim'
Bundle 'wavded/vim-stylus'
Bundle 'kchmck/vim-coffee-script'
Bundle 'alfredodeza/jacinto.vim'
Bundle 'sjl/badwolf'
Bundle 'altercation/vim-colors-solarized'
Bundle 'tomasr/molokai'
Bundle 'zaiste/Atom'
Bundle 'w0ng/vim-hybrid'

let g:slime_target = "tmux"
set wildignore+=*.pyc

"Filetype plugin indent on is required by vundle
filetype plugin indent on

nnoremap <silent> <leader>gs :Gstatus<CR>
nnoremap <silent> <leader>gd :Gdiff<CR>
nnoremap <silent> <leader>gc :Gcommit<CR>
nnoremap <silent> <leader>gb :Gblame<CR>
nnoremap <silent> <leader>gl :Glog<CR>
nnoremap <silent> <leader>gp :Git push<CR>
